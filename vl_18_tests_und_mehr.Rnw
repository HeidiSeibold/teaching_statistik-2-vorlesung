\documentclass[12pt,usenames,dvipsnames]{beamer}

\input{./slides_input/head.tex}

\newcommand{\Bias}{\text{Bias}}
\newcommand{\MSE}{\text{MSE}}

% tikz
\usepackage{tikz}
\usetikzlibrary{decorations.pathreplacing}

%--- Soll Lösung angezeigt werden?
\usepackage{ifthen}
\newboolean{solution}
\setboolean{solution}{false} % ja = true, nein = false
%---


%% figures path
\graphicspath{{figure/}}


%% Title
\author{Heidi Seibold}
\title{Tests\\ Erweiterungen und Probleme}
\date{Foliensatz 18\\ Sommersemester 2019}
\institute[]{Statistik II für\\ Studierende der Soziologie\\ und Nebenfachstudierende, LMU}

\begin{document}

<<setup, echo=FALSE, message=FALSE>>=
library("knitr")
opts_chunk$set(echo=FALSE, 
    fig.height=2, 
    fig.width=4, 
    out.width="\\textwidth",
    fig.show='hold',
    cache=FALSE)
library("ggplot2")
theme_set(theme_bw() + theme(plot.title = element_text(size = 5)))
@



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%-------------------------------------------------------------------------%
\thispagestyle{empty}


\begin{frame}
\maketitle
\end{frame}



\begin{frame}{Lernziele für heute}
\small
\begin{itemize}
\item Wissen, dass es Tests für Verteilungsannahmen, Unabhängigkeit von ZVs, etc. gibt. Problematik bei diesen Tests erklären können (man ist an der Nullhypothese interessiert).
\item Konfidenzintervalle als \textit{Tests} interpretieren können. 
\item Begründen können, warum Konfidenzintervalle nützlicher sind als Tests 
(Signifikanz versus Relevanz).
\item Erklären können, was das Problem beim multiplen Testen ist.
\item Erkennen können, wenn ein multiples Testproblem besteht und wissen was in dem Fall zu tun ist.
\end{itemize}
\end{frame}



\begin{frame}
\centering
\includegraphics[width=\textwidth]{vl_16_testen}
Tests können für verschiedenste Fragestellungen genutzt werden.
\end{frame}



\begin{frame}{Mögliche Alternativhypothesen $H_1$}
\begin{itemize}
\item AkademikerInnen sind schlechter im Handwerken als Nicht-AkademikerInnen.
\item In München regnet es im Mittel an mehr als 9 Tagen im Monat.
\item Die Varianz der Biermenge im Krug ist im Hackerzelt größer als im 
Augustinerzelt.
\item ...
\end{itemize}
Erinnerung: $H_1$ ist was wir zeigen wollen.
\end{frame}



\begin{frame}{Mögliche Alternativhypothesen $H_1$}
\begin{itemize}
\item Nach der Geburt eines Kindes schlafen Väter weniger als vor der Geburt.
\item Egoismus und Einkommen sind korreliert.
\item Das Einkommen folgt keiner Normalverteilung.
\item ...
\end{itemize}
Erinnerung: $H_1$ ist was wir zeigen wollen.
\end{frame}



\begin{frame}{Warum kann nur $H_1$ gezeigt werden?}
Manchmal wird $H_0$ nur deshalb nicht abgelehnt, weil unser Datensatz zu klein
ist oder unser Test nicht besonders geeignet.\\[2em]
$\Rightarrow$ kleine Power.\\
\centering
\includegraphics[width=0.3\textwidth]{vl_18_power} {\huge $\downarrow$}
\end{frame}



\begin{frame}{Unmögliche Alternativhypothesen $H_1$}
\begin{itemize}
\item Nach der Geburt eines Kindes schlafen Väter exakt gleich viel wie vor der Geburt.
\item Egoismus und Einkommen sind unabhängig.
\item Das Einkommen folgt einer Normalverteilung.\\[2em]
\end{itemize}
\textbf{
Problem:\\ 
\begin{center}
Manchmal möchte man Gleichheit zeigen\\
$\Downarrow$\\ 
Nicht möglich!
\end{center}
}
\end{frame}



\begin{frame}{Unmögliche Alternativhypothesen $H_1$}
\centering
Achtung!!!\\[2em]
Manchmal möchten Leute beweisen, dass die Normalverteilungsannahme angemessen
ist. Dafür nutzen sie einen Test.\\[2em]
\textbf{Warum ist das falsch?}
\end{frame}



\begin{frame}{Unmögliche Alternativhypothesen $H_1$}
\begin{itemize}
\item Nach der Geburt eines Kindes schlafen Väter exakt gleich viel wie vor der Geburt.
\item Egoismus und Einkommen sind unabhängig.
\item Das Einkommen folgt einer Normalverteilung.\\[2em]
\end{itemize}

Die Verteilung der Teststatistik unter $H_0$ muss bekannt sein oder geschätzt 
werden können.\\
\textbf{Formulieren Sie die Nullhypothesen zu den o.g. Alternativhypotesen. 
Warum ist die Verteilung der Teststatistik unter $H_0$ unbekannt?}

\end{frame}




\begin{frame}{Signifikanz}

Wenn $H_0$ verworfen werden kann, spricht man von einem \textbf{signifikanten}
Ergebnis.\\[2em]
\begin{itemize}
\item AkademikerInnen sind \textbf{signifikant} schlechter im Handwerken als Nicht-AkademikerInnen.
\item In München regnet es im Mittel an \textbf{signifikant} mehr als 9 Tagen im Monat.
\item Die Varianz der Biermenge im Krug ist im Hackerzelt \textbf{signifikant}
größer als im Augustinerzelt.
\end{itemize}

\end{frame}





\begin{frame}{Signifikanz}

Wenn $H_0$ verworfen werden kann, spricht man von einem \textbf{signifikanten}
Ergebnis.\\[4em]
\includegraphics[width=1\textwidth]{vl_18_significant} 
\footnotesize\url{https://xkcd.com/539}

\end{frame}


\begin{frame}{Signifikanz versus Relevanz}
Nach der Geburt eines Kindes schlafen Väter \textbf{signifikant} weniger als vorher.\\

\includegraphics[width=.5\textwidth]{vl_18_schlaf} \footnote{\url{https://flic.kr/p/e5V2Ko}}

Aber wie viel?\\[2em]

1 Minute? 120 Minuten?
\end{frame}







\begin{frame}{Signifikant und Relevant}
Nach der Geburt eines Kindes schlafen Väter \textbf{signifikant} weniger als vorher.\\

\includegraphics[width=.5\textwidth]{vl_18_schlaf} \footnote{\url{https://flic.kr/p/e5V2Ko}}

\textbf{Ab wann ist es ein relevanter Unterschied?}
\end{frame}




\begin{frame}{Signifikant und Relevant}
\centering
\huge
Test + Punktschätzer betrachten! (nicht nur Test)
\end{frame}


\begin{frame}{Signifikant und Relevant}

\begin{columns}
\begin{column}{0.5\textwidth}
<<fig.width=2, fig.height=3>>=
set.seed(42)
ns <- 500
vorher <- round(rnorm(n = ns, mean = 8, sd = 0.5), 2)
nachher <- round(rnorm(n = ns, mean = vorher - 0.1, sd = 0.4), 2)
schlafw <- data.frame(
    Vater = as.character(1:ns),
    vorher = vorher,
    nachher = nachher
    )

library("reshape2")
schlafl <- melt(schlafw, id.vars = c("Vater"), variable.name = "Zeitpunkt",
    value.name = "Schlafdauer")

ggplot(schlafl) +
    geom_line(aes(x = Zeitpunkt, y = Schlafdauer, group = Vater), 
        alpha = 0.1)
@

\end{column}
\begin{column}{0.5\textwidth}
<<fig.width=2>>=
schlafw$diff <- schlafw$vorher - schlafw$nachher
ggplot(schlafw, aes(y = vorher - nachher, x = 1)) + 
    geom_violin() +
    geom_jitter(alpha = 0.4, size = .7, width = 0.4, height = 0) +
    geom_hline(yintercept = mean(schlafw$diff), color = 2, linetype = 1) +
    geom_hline(yintercept = 0, color = 1, linetype = 3) +
    theme(panel.grid.major.x = element_blank(), 
        panel.grid.minor.x = element_blank(), 
        axis.text.x = element_blank(), 
        axis.ticks.x = element_blank(),
        axis.title.x = element_blank())
@
Roter Strich = arithmetisches Mittel\\[.5em]
\end{column}
\end{columns}
\end{frame}



\begin{frame}[fragile]
\footnotesize
Achtung: hier haben wir einen gepaarten t-Test (nicht zweistichproben t-Test)! 
<<echo=TRUE>>=
t.test(x = vorher, y = nachher, alternative = "greater", 
    paired = TRUE)
@
\textbf{Gibt es einen signifikanten Unterschied?\\
Gibt es einen relevanten Unterschied?}
\end{frame}



\begin{frame}{Signifikant und Relevant}
\centering
\huge
Alternativ: Konfidenzintervall statt Test nutzen
\end{frame}


\begin{frame}{Konfidenzintervalle als Alternative zum Test}
Beispiel:\\
Bootstrap-Konfidenzintervall für $T = \frac{1}{n} \sum\limits_{i=1}^n (x_{\text{vorher},i} - x_{\text{nachher},i})$
<<>>=
library("boot")
set.seed(23)
mean_diff <- function(data, indices) {
    d <- data[indices, ]
    mean(d$vorher - d$nachher)
}
B <- 1000
bs <- boot(data = schlafw, statistic = mean_diff, R = B)
bsdat <- data.frame(T0 = c(bs$t0, bs$t), 
    Daten = c("Original", rep("BSS", times = B)))
ggplot(data = bsdat, aes(y = T0, x = Daten)) + 
    geom_jitter(alpha = 0.4, size = .7, width = 0.4, height = 0) +
    # geom_hline(yintercept = mean(schlafw$diff), color = 2, linetype = 1) +
    geom_hline(yintercept = 0, color = 1, linetype = 3) +
    ylab("T")
@
\end{frame}


\begin{frame}
\small
$T = \frac{1}{n} \sum\limits_{i=1}^n (x_{\text{vorher},i} - x_{\text{nachher},i})$\\[.5em]
$H_0$: Väter schlafen nach Geburt des Kindes gleich viel wie vorher ($T \leq 0$)\\
$H_0$: Väter schlafen nach Geburt des Kindes weniger als vorher ($T > 0$)
\begin{columns}
\begin{column}{0.6\textwidth}
<<fig.height=3.5>>=
bsdata <- subset(bsdat, Daten == "BSS")

gamma <- c(0.99)
links <- (1 - gamma)/2
rechts <- 1 - links
pr <- c(links, rechts)
qs <- data.frame(gamma = rep(gamma, times = 2), pr = pr, 
    x = quantile(bsdata$T0, probs = pr))


ggplot(bsdata, aes(T0)) + stat_ecdf(geom = "step") +
    geom_segment(data = qs, aes(y = pr, yend = pr, x = 0, xend = x),
        color = 2, linetype = 1, alpha = 0.7) +
    geom_segment(data = qs, aes(y = 0, yend = pr, x = x, xend = x), 
        color = 2, linetype = 1, alpha = 0.7) +
    labs(y = expression(hat(F)(T^{BSS})), x = expression(T^{BSS}))
@
\end{column}
\begin{column}{0.4\textwidth}


99\%-Konfidenzintervall:

<<fig.width=2, fig.height=0.8>>=
library("reshape2")
qs$type  <- c("lower", "upper")
pqs <- dcast(qs, gamma ~ type, value.var = "x")
pqs$t0 <- bs$t0
pki <- ggplot(pqs) +
    geom_errorbar(aes(x = 1, ymin = lower, ymax = upper), color = 2, size = 2) +
    geom_point(aes(x = 1, y = t0), size = 4) +
    labs(y = "BS-Konfidenzintervall für T", x = "") +
    theme(panel.grid.major.y = element_blank(), 
        panel.grid.minor.y = element_blank(), 
        axis.text.y = element_blank(), 
        axis.ticks.y = element_blank(),
        axis.title.y = element_blank()) + 
    coord_flip()
pki
@

\vspace{2em}
Kann $H_0$ verworfen werden?
\end{column}
\end{columns}
\end{frame}




\begin{frame}[fragile]
\small
<<echo=TRUE>>=
boot.ci(bs, conf = 0.99, type = "basic")
@

Kann $H_0$ verworfen werden?
\end{frame}




\begin{frame}
\small
<<>>=
pki + geom_hline(yintercept = 0, linetype = 3)
@

$H_0$ kann verworfen werden, da dass Konfidenzintervall die 0 nicht überdeckt. Wir sind zu $99\%$ sicher, dass die wahre erwartete Differenz in Schlaflänge nicht 0 ist.
\end{frame}



\begin{frame}{Konfidenzintervalle als Alternative zum Test}
\centering
Konfidenzintervalle können oft als Tests genutzt werden.\\[2em]
$\alpha = 1 - \gamma$\\[2em]
Achtung: Konfidenzintervalle wie wir sie bisher berechnet haben sind immer wie ein zweiseitiger Test.
\end{frame}







\begin{frame}
\centering \huge

{Multiples Testen}
\end{frame}



\begin{frame}
\centering 
\includegraphics[height=\textheight]{vl_18_jb1}
\end{frame}



\begin{frame}
\centering 
\includegraphics[height=\textheight]{vl_18_jb2}
\end{frame}



\begin{frame}
\centering 
\includegraphics[height=0.8\textheight]{vl_18_jb3} {\footnotesize \url{xkcd.com/882}}\\
Was ist das Problem?\\
Wie hängt das Problem mit dem Konfidenzniveau zusammen?
\end{frame}



\begin{frame}{Multiples Testen}
Angenommen, es gibt 100 Farben bei den Jelly Beans und bei jeder Farbe wird folgende $H_0$ getestet:\\
\textit{Der Verzehr von Jelly Beans verursacht keine Akne.}\\[2em]
\textbf{Bei wie vielen der Tests erwarten wir ein signifikantes Ergebnis, auch wenn in Wahrheit kein Zusammenhang zwischen Akne und Jelly Beans besteht?}
\end{frame}



\begin{frame}
\textbf{Bei wie vielen der Tests erwarten wir ein signifikantes Ergebnis, auch wenn in Wahrheit kein Zusammenhang zwischen Akne und Jelly Beans besteht?}\\[2em]

Bei $\alpha\%$\\[2em]

\textbf{Warum?}

\end{frame}



\begin{frame}
\begin{align*}
\alpha =& \text{ Signifikanzniveau}\\
=& \text{ Wahrscheinlichkeit für $\alpha$-Fehler}\\
=& \text{ Wahrscheinlichkeit die Nullhypothese abzulehnen,}\\ 
~& \text{ obwohl sie in Wirklichkeit doch gilt.}\\
\end{align*}
Sei $\alpha = 5\%$ und $H_0$ wahr\\
Bei 100 Tests ist zu erwarten, dass bei 5 $H_0$ abgelehnt wird.
\end{frame}





\end{document}