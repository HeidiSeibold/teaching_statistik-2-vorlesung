RNW_FILES= $(wildcard *.Rnw)

all: vl_01_stat2soz_intro.pdf \
vl_02_stat2soz_whkeit.pdf \
vl_03_stat2soz_whkeit_rechnen.pdf \
vl_04_stat2soz_whkeit_zusammenfassung.pdf \
vl_05_stat2soz_zv.pdf \
vl_06_stat2soz_fragen.pdf \
vl_07_stat2soz_verteilung.pdf \
vl_08_stat2soz_verteilungen_beschreiben.pdf \
vl_09_stat2soz_mehrere_zv.pdf \
vl_10_induktive_intro.pdf \
vl_11_fragen.pdf \
vl_12_punktschaetzung.pdf \
vl_13_punktschaetzung2.pdf \
vl_14_punktschaetzung3.pdf \
vl_15_intervallschaetzung.pdf \
vl_16_testen_intro.pdf \
vl_17_tests.pdf \
vl_18_tests_und_mehr.pdf \
vl_19_gute_praxis.pdf \
vl_20_letzte.pdf


%.pdf: %.tex
	latexmk -pdf $<

%.tex: %.Rnw
	R -q -e "library(knitr);  knit('$<')"


clean:
	rm -f *~ \#*
	rm -f vl_*-concordance.tex \
	 *.nav \
	 *.snm \
	 *.synctex.gz \
	 vl_*.tex \
	 *.vrb \
	 *.fdb_latexmk \
	 *.aux \
	 *.fls \
	 *.log \
	 *.out \
	 *.toc
	find . -name \*unnamed-chunk-*.pdf -type f -delete



