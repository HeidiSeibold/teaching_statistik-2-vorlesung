\documentclass[12pt,usenames,dvipsnames]{beamer}

\input{./slides_input/head.tex}

\newcommand{\Bias}{\text{Bias}}
\newcommand{\MSE}{\text{MSE}}

% tikz
\usepackage{tikz}
\usetikzlibrary{decorations.pathreplacing}

%--- Soll Lösung angezeigt werden?
\usepackage{ifthen}
\newboolean{solution}
\setboolean{solution}{false} % ja = true, nein = false
%---


%% figures path
\graphicspath{{figure/}}


%% Title
\author{Heidi Seibold}
\title{Gute wissenschaftliche Praxis\\ (ein Ausschnitt)}
\date{Foliensatz 19\\ Sommersemester 2019}
\institute[]{Statistik II für\\ Studierende der Soziologie\\ und Nebenfachstudierende, LMU}

\begin{document}

<<setup, echo=FALSE, message=FALSE>>=
library("knitr")
opts_chunk$set(echo=FALSE, 
    fig.height=2, 
    fig.width=4, 
    out.width="\\textwidth",
    fig.show='hold',
    cache=FALSE)
library("ggplot2")
theme_set(theme_bw() + theme(plot.title = element_text(size = 5)))
@



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%-------------------------------------------------------------------------%
\thispagestyle{empty}


\begin{frame}
\maketitle
\end{frame}



\begin{frame}{Lernziele für heute}
\small
\begin{itemize}
\item p-Hacking in einem Projekt erkennen und dagegen argumentieren können.
\item Publikationsbias erklären können und begründen können, warum er ein Problem darstellt.
\item Argumentieren können, warum Präregistrierung sinnvoll ist.
\end{itemize}
\end{frame}


\begin{frame}{Das Problem mit den Fehlern...}

Dieses Video zeigt grafisch sehr schön, warum die Fehler so problematisch sind.

\url{https://youtu.be/TosyACdsh-g}

\end{frame}



\begin{frame}{Die Reproduzierbarkeitskrise}
\url{https://www.nature.com/news/1-500-scientists-lift-the-lid-on-reproducibility-1.19970}
\end{frame}





\begin{frame}{p-Hacking}
Forscher, die um jeden Preis einen kleinen p-Wert wollen, begehen manchmal p-Hacking.\\
Sie spielen so lange an der Fragestellung oder den Daten herum, bis ein signifikanter Effekt herauskommt.\\[2em]
\textbf{Warum ist das ein Problem?}
\end{frame}





\begin{frame}{Publikationsbias}
Oft publizieren Forscher ihre Ergebnisse nur, wenn sie interessante Ergebnisse finden (z.B. signifikante Unterschiede zwischen Gruppen).\\[2em]
\textbf{Warum ist das ein Problem? Warum führt das zu einem Bias?}
\end{frame}




\begin{frame}{Publikationsbias + p-Hacking}
\textbf{Eine mögliche Lösung: Präregistrierung}\\[1em]
Man sagt vorher was man machen wird (Fragestellung, Hypothesen, Analysen).
\end{frame}




\end{document}