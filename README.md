# Statistik II für Studierende der Soziologie und Nebenfachstudierende
Heidi Seibold
Sommersemester 2019


## Lizenz

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.


Mehr Informationen befinden sich im [LICENSE](LICENSE)-File.


## Lernziele

### Einleitung

#### Statistik Grundbegriffe 
- Verstehen: Was ist deskriptive Statistik? Was ist induktive Statistik? Was
sind explorative Analysen? Was sind confirmatorische Analysen?
- Erklären können, was der Unterschied zwischen deskriptiver und induktiver Statistik ist.
- Erklären können, was der Unterschied zwischen explorativen und confirmatorischen Analysen ist. 


### Wahrscheinlichkeitsrechnung 

#### Rechnen mit Wahrscheinlichkeiten
- Rechenregeln anwenden können.
- Wahrscheinlichkeiten interpretieren können [VL + Ü]
- Prinzip der Laplace-Wahrscheinlichkeit verstehen und beschreiben können
- Den Begriff "stochastische Unabhängigkeit" verstehen und beim Rechnen
  anwenden können [VL + Ü]. 
- Satz von Bayes beim Rechnen nutzen können [VL +  Ü].
- Satz der totalen Wahrscheinlichkeit beim Rechnen anwenden können.
- Mit bedingten Wahrscheinlichkeiten rechnen können [VL + Ü].
- Textaufgaben zu bedingter Wahrscheinlichkeit lösen können. 
- Bedingte Wahrscheinlichkeiten in Wahrscheinlichkeitsbaum eintragen können
- Bedingte Wahrscheinlichkeiten in Texten (z.B. Zeitungsartikeln) erkennen und
  problematische Bedingungen erkennen. Beispiel: Wahrscheinlichkeit, dass ein
[Fahrradfahrer schuld ist an einem
Fahrradunfall](https://www.merkur.de/lokales/erding/erding-ort28651/polizei-meistens-sind-radler-an-unfaellen-selbst-schuld-9893134.html) 
- Bedingte Wahrscheinlichkeiten interpretieren können [VL + Ü].



#### Zufallsvariablen
- Erklären können was eine Zufallsvariable ist.
- Benennen können was mögliche Realisierungen von ZVs sind. Beispiel: Würfel 1,
  ..., 6
- Zufallsvariablen characterisieren können: stetig, diskret, kategoriell,
  binär.
- Verstehen, dass bei der Messung stetiger Merkmale, immer diskrete Messwerte
  erhoben werden.
- Bestimmen können, wenn ZV (nicht) unabhängig sind. Beispiel: Zwei Spiele mit
  gleichen Spielern; Gewinnen beim Würfelwurf versus Gewinnen beim Schach

#### Dichtefunktion und Verteilungsfunktion [VL + Ü]
- Verstehen was Dichtefunktionen und Verteilungsfunktionen sind.
- Dichtefunktionen und Verteilungsfunktionen interpretieren können. Beispiel:
  Wert des Medians oder anderer Quantile, Vergleich von Varianzen verschiedener
Dichtekurven, Wahrscheinlichkeit dass ZV zwischen zwei Werten.
- Wichtige Verteilungen benennen können (Normalverteilung, Binomialverteilung,
  Poissonverteilung).
- Eine Vorstellung davon haben wie Dichtekurven verschiedener Verteilungen
  aussehen. Beispiel: Bilder von zwei Dichtekurven zuordnen zu
"Normalverteilung" oder "Poissonverteilung" zuordnen.
- Bestimmen können, wenn ZV (nicht) die gleiche Verteilung haben.
- Einschätzend können, ob ZV i.i.d. sind.

#### Erwarungswert, Varianz, Kovarianz und Korrelation [VL + Ü]
- Konzept von Erwartungswert, Varianz, Kovarianz und Korrelation erklären können
- Mit Erwartungswerten und Varianzen rechnen können (Rechenregeln anwenden können).

#### Sonstiges aus Übung
- Verstehen, warum man für Lebensdauer kompliziertere Modelle braucht
- Grenzwertsätze und Approximation


### Induktive Statistik

#### Grundprinzipien der induktiven Statistik
- Erklären können was eine Stichprobe ist und wie sie mit der Grundgesamtheit zusammen hängt.
- Gütekriterien (Zufallsauswahl) für Stichproben benennen können und bei
einfachen Beispielen prüfen können, ob die Stichprobe gut ist.
- Beschreiben können warum grosse Stichproben bei der Schätzung helfen.
- Beschreiben können warum Datenqualität eine wichtige Rolle bei der Schätzung spielt.

#### Punktschätzung
- Erklären können, was ein Schätzer ist.
- Erklären können, was der Unterschied zwischen einem Schätzer und einer Kenngrösse ist.
- Erklären können, warum Schätzer zufällig sind.
- Schätzer für Erwarungswert, Varianz, Kovarianz, Korrelation und MSE berechnen und
  interpretieren können. [VL + Ü]
- Schätzer für Quantile berechnen und interpretieren können.
- Masszahlen für Schätzer (Erwartungstreue und Bias) erklären können.
- Für bestimtme Fälle (e.g. Stichprobe nicht i.i.d.) begründen können, warum
  Schätzer nicht das tut was er soll.

#### Intervallschätzung
- Das Konzept von Konfidenzintervallen erklären können [T-a, T+a]
- Konfidenzintervall für den Mittelwert eines normalverteilten Merkmals
  berechnen und interpretieren können.
- Konfidenzintervall für Anteile berechnen und interpretieren können [VL+ Ü].
- Berechnung des Stichprobenumfangs [Ü]

#### Hypothesentests
- Hypothesen für verschiedene Forschungsfragen definieren können.[VL + Ü] 
- Testfehler erklären können.[VL + Ü]
- Begründen können warum man nur einen Fehler kontrollieren kann. [VL + Ü]
- Erklären können was das Signifikanzniveau ist.[VL + Ü]
- Streit über Höhe des Signifikanzniveaus in der Forschung zusammenfassen
  können.
- Situationen benennen können in denen ein t-Test/Test für Anteilswerte (nicht)
  sinnvoll ist.
- t-Test/Test für Anteilswerte berechnen können (mit Tabelle)
  und Ergebnis interpretieren können (wenn selbst berechnet oder von Software Output). [VL + Ü]
- Wissen, dass es Tests für Verteilungsannahmen, Unabhängigkeit von ZVs, … gibt. Problematik bei diesen Tests erklären können (man ist an der Nullhypothese interessiert). [VL + Ü]
- Konfidenzintervalle als "Tests" interpretieren können. [VL + Ü]
- Begründen können, warum Konfidenzintervalle nützlicher sind als Tests
  (Signifikanz versus Relevanz). [VL + Ü] 
- In Publikationen erkennen ob die Verwendung eines Konfidenzintervalls anstatt
  eines p-Werts möglich gewesen wäre.
- In Publikationen nachvollziehen können welcher Test gerechnet wurde und
  welche Annahmen dahinter stehen (ggf. unter Verwendung von weiterer
Literatur).
- Erklären können, was das Problem beim multiplen Testen ist.
- Erkennen können, wenn ein multiples Testproblem besteht und wissen was in dem
  Fall zu tun ist.



#### Gute Praxis
- p-Hacking in einem Projekt erkennen und dagegen argumentieren können.
- Publikationsbias erklären können und begründen können, warum er ein Problem
  darstellt.
- Argumentieren können, warum Präregistrierung sinnvoll ist.



## Ressourcen

- Online-Material: [Crashkurs Statistik](https://www.crashkurs-statistik.de) von Alexander Engelhardt beschreibt die meisten Inhalte der Vorlesung verständlich und kurz.
- Videos: [Kurzes Tutorium Statistik](https://www.youtube.com/channel/UCtBEklAtHHji2V1TsaTzZXw) von Mathias Bärtl



## Erwartete Vorkenntnisse

### Statistik I 

#### Kapitel 1: Formalisierung 
- Skalenniveaus; Stetige, quasi-stetige und diskrete Merkmale

#### Kapitel 2: Häufigkeitsverteilungen
- Grafische Darstellungen (insb. Histogramme), kummulierte Häufigkeiten, empirische Verteilungsfunktion

#### Kapitel 3: Lage - und Streuungsmaße
- Arithm. Mittel, Standardabweichung und Varianz, 
	           Transformationen,	
	           Streuungszerlegung,
               Median, Quantile, Modus,
               Geom., Harmonisches Mittel,
               Variationskoeffizient, 
               Box-Plot

#### Kapitel 4: Konzentrations-Armutsmessung          
- Relative Konzentration,
             Lorenzkurve, Gini-Koeffizient, auch quantilsbezogen 
			 RHI,
	         Palma-Quotient,
		     absolute Konzentration,
			 Konzentrationsrate und Herfindahlindex/Anzahl effektiver Marktteilnehme
            
#### Kapitel 5: Kontingenztabelle; Tabelle mit gemeinsamen/bedingten Häufigkeiten,
- emp. Unabhängigkeit,
	  Chi-Quadrat + darauf aufbauende Maßzahlen (+Interpretation),
	  Vier-Felder-Tafel: Maßzahlen (+Interpretation),
	  Ökologischer Fehlschluss,
	  PRE-Maße,
	  ordinale Maßzahlen

#### Kapitel 6: Korrelation (Pearson,Spearman)
- Lineare Einfachregression,
  Multiple Lineare Regression,
  Varianzanalyse,  

#### Software: Interpretation von R Output
