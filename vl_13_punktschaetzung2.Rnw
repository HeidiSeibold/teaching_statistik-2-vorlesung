\documentclass[12pt,usenames,dvipsnames]{beamer}

\input{./slides_input/head.tex}

\newcommand{\Bias}{\text{Bias}}
\newcommand{\MSE}{\text{MSE}}

% Images
\usepackage{marvosym}

%--- Soll Lösung angezeigt werden?
\usepackage{ifthen}
\newboolean{solution}
\setboolean{solution}{false} % ja = true, nein = false
%---


%% figures path
\graphicspath{{figure/}}


%% Title
\author{Heidi Seibold}
\title{Punktschätzung 2}
\date{Foliensatz 13\\ Sommersemester 2019}
\institute[]{Statistik II für\\ Studierende der Soziologie\\ und Nebenfachstudierende, LMU}

\begin{document}

<<setup, echo=FALSE, message=FALSE>>=
library("knitr")
opts_chunk$set(echo=FALSE, 
    fig.height=1.5, 
    fig.width=4, 
    out.width="0.85\\textwidth",
    fig.show='hold',
    cache=TRUE)
library("ggplot2")
theme_set(theme_bw() + theme(plot.title = element_text(size = 5)))
@



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%-------------------------------------------------------------------------%
\thispagestyle{empty}


\begin{frame}
\maketitle
\end{frame}


\begin{frame}{Klausur}
\large
Termin: 27.07.2019, 14:00 Uhr c.t.\\[2em]
Raum: Wird nach Ablauf der Anmeldephase bekannt gegeben\\
\end{frame}




\begin{frame}{Erlaubte Hilfsmittel}
\begin{itemize}
    \item Eine eigenständige, selbstgeschriebene, nicht kopierte oder zusammengeklebte Formelsammlung mit 4 DIN A4 Seiten (doppelseitig beschrieben) oder 8 DIN A4 Seiten (einseitig beschrieben). 
    \item Allgemeines Wörterbuch bei Studierenden, deren Muttersprache nicht Deutsch ist (Deutsch-Muttersprache / Muttersprache-Deutsch).
    \item Ein nicht-programmierbarer und nicht-grafikfähiger Taschenrechner.
\begin{itemize}
    \item Taschenrechner, die auch für das Bayrischen Abitur zugelassen sind.
    \item z.B. \url{https://www.casio-schulrechner.de/de/produkte/zulassungsrichtlinien}
\end{itemize}
\end{itemize}
\end{frame}




\begin{frame}{Klausuranmeldung}
\begin{itemize}
    \item Studierende der Soziologie: Anmeldung über das LSF, Anmeldezeitraum wird vom PAGS veröffentlicht.

    \item Nebenfach Statistik (nicht Hauptfach Soziologie): Anmeldung über Moodle (wird erst im Laufe des Semesters freigeschalten); Bitte beachten Sie unbedingt die neuen Regelungen zur Klausuranmeldung

    \item Alle anderen: Bitte klären Sie eine eventuell notwendige Anmeldung bei dem Prüfungsamt, das den Studiengang verwaltet, für den die Leistung eingebracht werden soll! Zudem wird eine Anmeldung über Moodle empfohlen. 
\end{itemize}
\end{frame}







\begin{frame}{Lernziele für heute}
\small
\begin{itemize}
\item Erklären können, was ein Schätzer ist.
\item MSE für Schätzer berechnen und interpretieren können.
\item Erklären können, warum Schätzer zufällig sind.
\item Masszahlen für Schätzer erklären können.
\end{itemize}
\end{frame}

\begin{frame}
\includegraphics[width=1\textwidth]{vl_12_theorie_daten}
Es ist unbekannt wie gut $\bar{x}$ ist, d.h. wie nah am wahren Erwartungswert $E(X)$
\end{frame}

\begin{frame}{Beispiel: Bier auf der Wiesn}
\centering
Du bist mit deinen Freunden auf dem Oktoberfest. Ihr alle bestellt eine Mass.\\
\includegraphics[width=0.2\textwidth, angle=-30]{vl_12_bier_m}
\includegraphics[width=0.2\textwidth, angle=20]{vl_12_bier}\\[1em]
Jeder der Krüge ist leicht anders befüllt.\\
\end{frame}





\begin{frame}
\centering

<<>>=
set.seed(123)
n <- 30
x <- round(rnorm(n = n, mean = 0.98, sd = 0.1), 2)
d <- data.frame(x = x)
@
Da muss eine Studie her!\\[1em]

<<results = "asis">>=
for(i in 1:10)
cat('\\includegraphics[width=0.1\\textwidth]{vl_12_bier}')

cat('\\\\')

for(i in 1:10)
cat('\\includegraphics[width=0.1\\textwidth]{vl_12_bier}')
@
\vspace{1.5em}\\
$X$: Menge an Bier im Krug (gemessen in l)\\[1em]
\end{frame}



\begin{frame}{Machen wir ein Spiel mit den Biertrinkern}
Wer ist im näher an der wirklichen Biermenge im Krug?\\[1em]
\begin{itemize}
\item Der Erwartungswertschätzer $\bar{x}$
\item Die Schätzung des jeweiligen Trinkers des Biers
\end{itemize}
\end{frame}


\begin{frame}{Machen wir ein Spiel mit den Biertrinkern}
<<message=FALSE, fig.width=2, fig.height=2, out.width="0.49\\textwidth">>=
set.seed(111)
d$xt <- round(sapply(d$x, function(xi) rnorm(n = 1, mean = xi, sd = 0.1)), 2)
d$meanx <- mean(d$x)
rx <- range(c(d$x, d$xt))
ggplot(d, aes(x = x, y = meanx)) + geom_point() + ylim(min(d$xt), max(d$xt)) + 
    labs(x = "Biermenge (l)", y = "Schätzung (l)\n Mittelwert") +
    geom_abline(slope = 1, color = 2) + lims(x = rx, y = rx)
ggplot(d, aes(x = x, y = xt)) + geom_point() + 
    labs(x = "Biermenge (l)", y = "Schätzung (l)\n Biertrinker") +
    geom_abline(slope = 1, color = 2) + lims(x = rx, y = rx)
@
Was bedeutet die rote Linie?\\
Wer gewinnt? Der Mittelwert oder die Biertrinker?
\end{frame}



\begin{frame}{Wie falsch liegen die Schätzer jeweils?}
Betrachten wir den Abstand zwischen Schätzer und wahrer Biermenge:
$x_i - \hat{x_i}$
<<fig.height=2.7>>=
dn1 <- d[, c("x", "xt") ]
names(dn1) <- c("wahr", "geschaetzt")
dn1$Schaetzer <- "Biertrinker"

dn2 <- d[, c("x", "meanx") ]
names(dn2) <- c("wahr", "geschaetzt")
dn2$Schaetzer <- "Mittelwert"

dn <- rbind(dn1, dn2)

dn$Fehler <- dn$wahr - dn$geschaetzt

ggplot(dn, aes(x = Schaetzer, y = Fehler, color = Schaetzer)) + geom_violin() +
    geom_jitter() + theme(legend.position = "none")
@
Wer gewinnt? Der Mittelwert oder die Biertrinker?
\end{frame}



\begin{frame}{Wie falsch liegen die Schätzer jeweils?}
Betrachten wir den quadratischen Abstand zwischen Schätzer und wahrer Biermenge:
$(x_i - \hat{x_i})^2$
<<fig.height=2.7>>=
dn$Fehler2 <- (dn$wahr - dn$geschaetzt)^2

ggplot(dn, aes(x = Schaetzer, y = Fehler2, color = Schaetzer)) + geom_violin() +
    geom_jitter() + theme(legend.position = "none")
@
Wer gewinnt? Der Mittelwert oder die Biertrinker?
\end{frame}

\begin{frame}{Wie falsch liegen die Schätzer jeweils?}
Betrachten wir den mittleren quadratischen Abstand zwischen Schätzer und wahrer Biermenge:
\begin{align*}
\MSE(x, \hat{x}) &= \frac{1}{n} \sum\limits_{i=1}^n (x_i - \hat{x_i})^2\\
&\\
\MSE(x, \hat{x}_\text{Biertrinker}) &= \Sexpr{format(round(mean(dn$Fehler2[dn$Schaetzer == "Biertrinker"]), 3), nsmall = 3)}
&\\
\MSE(x, \hat{x}_\text{Mittelwert}) &= \Sexpr{round(mean(dn$Fehler2[dn$Schaetzer == "Mittelwert"]), 3)}
\end{align*}
Wer gewinnt? Der Mittelwert oder die Biertrinker?
\end{frame}


\begin{frame}{MSE}

MSE\\
= Mean Squared Error\\
= Mittlerer quadratischer Fehler\\
= Mittlerer quadratischer Abstand zwischen Schätzer\\ ~~~ und Wahrheit\\[2em]

In Beispiel gab es 2 Schätzer:\\
(1) Mittelwert / Arithmetisches Mittel (Punktschätzer für Erwartungswert)\\
(2) Persönliche Ein\textbf{schätz}ung der Biertrinker
\end{frame}



\begin{frame}{MSE}{Wofür braucht man den?}
Du gehst mit den Ergebnissen der Bierstudie zum Wiesnwirt.\\
Dieser Bestreitet, dass deine Studie zum richtigen Ergebnis gekommen ist und
behauptet im Mittel wird ganz sicher 1 Liter eingeschenkt.\\

\hspace{6em}\includegraphics[width=0.7\textwidth]{vl_13_wirt}

Was nun?
\end{frame}




\begin{frame}
\hspace{6em}\includegraphics[width=0.7\textwidth]{vl_13_wirt}\\[1em]

Gegen den Vorwurf, dass der Schätzer ungeeignet ist, kann man zeigen, dass der
Mittelwert i.d.R. ein guter Schätzer für den Erwartungswert ist:
\begin{itemize}
\item Noch größere Studie machen und Fehler (MSE) berechnen (Schätzer Wirt: 1l, Schätzer Mittlewert: 0.98l)
\item Theoretische Eigenschaften des Schätzers
\end{itemize}
\end{frame}




\begin{frame}
\begin{itemize}
\item Noch größere Studie machen und Fehler berechnen\\
$\Rightarrow$ Schätzwert evaluieren
\item Theoretische Eigenschaften des Schätzers\\
$\Rightarrow$ Schätzverfahren evaluieren
\end{itemize}
\end{frame}




\begin{frame}{Ist mein Schätzer geeignet?}{Theoretische Eigenschaften}

Es gibt verschiedene Kriterien:
\begin{itemize}
\item \textbf{Erwartungstreue}: findet mein Schätzer im Mittel den richtigen Wert?
\item \textbf{Bias}: ist mein Schätzer unverzerrt?
\item \textbf{Effizienz}: hat mein Schätzer eine geringe Varianz?
\item \textbf{Theoretischer MSE}
\end{itemize}

\end{frame}


\begin{frame}{Beispiel: Wie viele Bücher?}
Grundgesamtheit: drei Personen (\textcolor{red}{Anna}, \textcolor{OliveGreen}{Max}, \textcolor{blue}{Eva})\\
Stichprobe: Zufallsstichprobe ($n=2$)\\
Zufallsvariable $X$: Anzahl an Büchern.
	\begin{center}
		\begin{tabular}{|c|c||c|c|}
			\hline
			             & Personen in & & \\
			Stichprobe & der Stichprobe& $X_1$ & $X_2$ \\
			\hline
			1 & \textcolor{red}{Anna}, \textcolor{OliveGreen}{Max} & 3 & 1 \\
			\hline
			2 & \textcolor{red}{Anna}, \textcolor{blue}{Eva} & 3 & 2 \\
			\hline
			3 & \textcolor{OliveGreen}{Max}, \textcolor{red}{Anna} & 1 & 3 \\
			\hline
			4 & \textcolor{OliveGreen}{Max}, \textcolor{blue}{Eva} & 1 & 2 \\
			\hline
			5 & \textcolor{blue}{Eva}, \textcolor{red}{Anna} & 2 & 3 \\
			\hline
			6 & \textcolor{blue}{Eva}, \textcolor{OliveGreen}{Max} & 2 & 1 \\
			\hline
		\end{tabular}
	\end{center}
	Wie viele Bücher haben Anna, Max und Eva jeweils?
\end{frame}


\begin{frame}{Beispiel: Wie viele Bücher?}
Forschungsfrage: Wie viele Bücher haben die Personen der Grundgesamtheit im
Mittel/Erwarungswert?\\
	\begin{center}
		\begin{tabular}{|c|c||c|c|}
			\hline
			             & Personen in & & \\
			Stichprobe & der Stichprobe& $X_1$ & $X_2$ \\
			\hline
			1 & \textcolor{red}{Anna}, \textcolor{OliveGreen}{Max} & 3 & 1 \\
			\hline
			2 & \textcolor{red}{Anna}, \textcolor{blue}{Eva} & 3 & 2 \\
			\hline
			3 & \textcolor{OliveGreen}{Max}, \textcolor{red}{Anna} & 1 & 3 \\
			\hline
			4 & \textcolor{OliveGreen}{Max}, \textcolor{blue}{Eva} & 1 & 2 \\
			\hline
			5 & \textcolor{blue}{Eva}, \textcolor{red}{Anna} & 2 & 3 \\
			\hline
			6 & \textcolor{blue}{Eva}, \textcolor{OliveGreen}{Max} & 2 & 1 \\
			\hline
		\end{tabular}
	\end{center}
Wahrer Erwartungswert: $\mu = 2$
\end{frame}


\begin{frame}
Forschungsfrage: Wie viele Bücher haben die Personen der Grundgesamtheit im
Mittel/Erwarungswert?\\[.5em]
$\Rightarrow$ $\mu$ schätzen
\\[2em]


Betrachte folgende m\"{o}glichen Sch\"{a}tzer:
\begin{eqnarray*}
 T_1 &=& \frac{X_1+X_2}{2}\\
 T_2 &=& X_1\\
 T_3 &=& \frac{2}{3} \cdot X_2
\end{eqnarray*}
\end{frame}

\begin{frame}{Beispiel: Wie viele Bücher?}
\begin{eqnarray*}
 T_1 &=& \frac{X_1+X_2}{2}\\
 T_2 &=& X_1\\
 T_3 &=& \frac{2}{3} \cdot X_2
\end{eqnarray*}
	\begin{center}
		\begin{tabular}{|c|c||c|c||c|c|c|}
			\hline
			&Personen in & \multicolumn{5}{|c|}{Realisationen von}\\
			Stichprobe & der Stichprobe& $X_1$ & $X_2$ & $T_1$ & $T_2$ & $T_3$\\
			\hline
			1 & \textcolor{red}{Anna}, \textcolor{OliveGreen}{Max} & \textcolor{red}{3} & \textcolor{OliveGreen}{1} & 2 & 3 & 0.6\\
			\hline
			2 & \textcolor{red}{Anna}, \textcolor{blue}{Eva} & \textcolor{red}{3} & \textcolor{blue}{2} & 2.5 & 3 & 1.3\\
			\hline
			3 & \textcolor{OliveGreen}{Max}, \textcolor{red}{Anna} & \textcolor{OliveGreen}{1} & \textcolor{red}{3} & 2 & 1 & 2\\
			\hline
			4 & \textcolor{OliveGreen}{Max}, \textcolor{blue}{Eva} & \textcolor{OliveGreen}{1} & \textcolor{blue}{2} & 1.5 & 1 & 1.3\\
			\hline
			5 & \textcolor{blue}{Eva}, \textcolor{red}{Anna} & \textcolor{blue}{2} & \textcolor{red}{3} & 2.5 & 2 & 2\\
			\hline
			6 & \textcolor{blue}{Eva}, \textcolor{OliveGreen}{Max} & \textcolor{blue}{2} & \textcolor{OliveGreen}{1} & 1.5 & 2 & 0.6\\
			\hline
		\end{tabular}
	\end{center}
\end{frame}



\begin{frame}{Erwartungstreue}
Ein Schätzer $T$ hei{\ss}t \textit{erwartungstreu f\"{u}r den Parameter $\vartheta$}, falls gilt
\[
 E(T)=\vartheta
\]
f\"{u}r alle $\vartheta$.\\[2em]
\underline{Hier:} $\vartheta = \mu$
\end{frame}



\begin{frame}{Erwartungstreue}
Ein Schätzer $T$ hei{\ss}t \textit{erwartungstreu f\"{u}r den Parameter $\mu$}, falls gilt
\[
 E(T)=\mu
\]
\begin{align*}
E(T_1) &= E\left(\frac{X_1+X_2}{2}\right)\\
E(T_2) &= E(X_1)\\
E(T_3) &= E\left(\frac{2}{3} \cdot X_2\right)\\
\end{align*}
\end{frame}


\begin{frame}
\begin{columns}
\begin{column}{0.7\textwidth}
		\begin{tabular}{|c|c||c|c|}
			\hline
			             & Personen in & & \\
			Stichprobe & der Stichprobe& $X_1$ & $X_2$ \\
			\hline
			1 & \textcolor{red}{Anna}, \textcolor{OliveGreen}{Max} & 3 & 1 \\
			\hline
			2 & \textcolor{red}{Anna}, \textcolor{blue}{Eva} & 3 & 2 \\
			\hline
			3 & \textcolor{OliveGreen}{Max}, \textcolor{red}{Anna} & 1 & 3 \\
			\hline
			4 & \textcolor{OliveGreen}{Max}, \textcolor{blue}{Eva} & 1 & 2 \\
			\hline
			5 & \textcolor{blue}{Eva}, \textcolor{red}{Anna} & 2 & 3 \\
			\hline
			6 & \textcolor{blue}{Eva}, \textcolor{OliveGreen}{Max} & 2 & 1 \\
			\hline
		\end{tabular}
\end{column}
\begin{column}{0.3\textwidth}
\centering
$X_1$ ist eine ZV mit:
\begin{align*}
P(X_1 = 1) &= \frac{1}{3}\\
P(X_1 = 2) &= \frac{1}{3}\\
P(X_1 = 3) &= \frac{1}{3}\\
\end{align*}
Warum?
\end{column}
\end{columns}
\end{frame}


\begin{frame}
\begin{columns}
\begin{column}{0.4\textwidth}
\vspace{0.6em}\\
$X_1$ ist eine ZV mit:
\begin{align*}
P(X_1 = 1) &= \frac{1}{3}\\
P(X_1 = 2) &= \frac{1}{3}\\
P(X_1 = 3) &= \frac{1}{3}\\
\end{align*}
$X_2$ ist eine ZV mit:
\begin{align*}
P(X_2 = 1) &= \frac{1}{3}\\
P(X_2 = 2) &= \frac{1}{3}\\
P(X_2 = 3) &= \frac{1}{3}\\
\end{align*}
\end{column}
%
\begin{column}{0.6\textwidth}
\textbf{Berechnen Sie}
\begin{itemize}
\item $E(X_1)$ 
\item $E(X_2)$
\end{itemize}
\vspace{4em}
\underline{Erinnerung}: $E(X)$ diskrete ZV
$$E(X) = \sum\limits_{x \in \mathcal{X}} x \cdot P(X = x)$$
\end{column}
\end{columns}
\end{frame}



\begin{frame}
\begin{columns}
\begin{column}{0.4\textwidth}
\vspace{0.6em}\\
$X_1$ ist eine ZV mit:
\begin{align*}
P(X_1 = 1) &= \frac{1}{3}\\
P(X_1 = 2) &= \frac{1}{3}\\
P(X_1 = 3) &= \frac{1}{3}\\
\end{align*}
$X_2$ ist eine ZV mit:
\begin{align*}
P(X_2 = 1) &= \frac{1}{3}\\
P(X_2 = 2) &= \frac{1}{3}\\
P(X_2 = 3) &= \frac{1}{3}\\
\end{align*}
\end{column}
%
\begin{column}{0.6\textwidth}
\textbf{Berechnen Sie}
\begin{itemize}
\item $E\left(\frac{2}{3} \cdot X_2\right)$
\end{itemize}
\vspace{4em}
\underline{Erinnerung}: Rechenregeln $E(X)$ 
\begin{itemize}
 \item $E(aX) = a \cdot E(X) \quad$\\ ($a$ ist eine Zahl) 
 \item $E(X+Y) =  E(X) + E(Y)$
 \item $E(aX+bY) = a\cdot E(X)+b\cdot E(Y)$ 
\end{itemize}
\end{column}
\end{columns}
\end{frame}




\begin{frame}
\begin{columns}
\begin{column}{0.4\textwidth}
\vspace{0.6em}\\
$X_1$ ist eine ZV mit:
\begin{align*}
P(X_1 = 1) &= \frac{1}{3}\\
P(X_1 = 2) &= \frac{1}{3}\\
P(X_1 = 3) &= \frac{1}{3}\\
\end{align*}
$X_2$ ist eine ZV mit:
\begin{align*}
P(X_2 = 1) &= \frac{1}{3}\\
P(X_2 = 2) &= \frac{1}{3}\\
P(X_2 = 3) &= \frac{1}{3}\\
\end{align*}
\end{column}
%
\begin{column}{0.6\textwidth}
\textbf{Berechnen Sie}
\begin{itemize}
\item $E\left(\frac{X_1+X_2}{2}\right)$
\end{itemize}
\vspace{4em}
\underline{Erinnerung}: Rechenregeln $E(X)$ 
\begin{itemize}
 \item $E(aX) = a \cdot E(X) \quad$\\ ($a$ ist eine Zahl) 
 \item $E(X+Y) =  E(X) + E(Y)$
 \item $E(aX+bY) = a\cdot E(X)+b\cdot E(Y)$ 
\end{itemize}
\end{column}
\end{columns}
\end{frame}




\begin{frame}{Erwartungstreue}
Ein Schätzer $T$ hei{\ss}t \textit{erwartungstreu f\"{u}r den Parameter $\mu$}, falls gilt
\[
 E(T)=\mu
\]
\begin{align*}
E(T_1) &= E\left(\frac{X_1+X_2}{2}\right) = 2 &= \mu \\
E(T_2) &= E(X_1) = 2 &= \mu \\
E(T_3) &= E\left(\frac{2}{3} \cdot X_2\right) = 1.3 &\neq \mu \\
\end{align*}
\end{frame}



\begin{frame}{Bias}
Die Gr\"{o}{\ss}e
\[
 \Bias(T)=E(T)-\vartheta
\]
hei{\ss}t \textit{Bias} (oder \textit{Verzerrung}) der Sch\"{a}tzfunktion. 
\end{frame}



\begin{frame}{Bias}
Die Gr\"{o}{\ss}e
\[
 \Bias(T)=E(T)-\mu
\]
hei{\ss}t \textit{Bias} (oder \textit{Verzerrung}) der Sch\"{a}tzfunktion. 

\begin{columns}
\begin{column}{0.4\textwidth}
\begin{align*}
E(T_1) &= E\left(\frac{X_1+X_2}{2}\right) = 2 = \mu \\
E(T_2) &= E(X_1) = 2 = \mu \\
E(T_3) &= E\left(\frac{2}{3} \cdot X_2\right) = 1.3 \neq \mu \\
\end{align*}
\end{column}
%
\begin{column}{0.6\textwidth}
\begin{align*}
\Bias(T_1) &= E\left(\frac{X_1+X_2}{2}\right) - \mu \\
\Bias(T_2) &= E(X_1) - \mu \\
\Bias(T_3) &= E\left(\frac{2}{3} \cdot X_2\right) - \mu \\
\end{align*}
\end{column}
\end{columns}
\end{frame}



\begin{frame}
\centering \Large
Schätzer erwartungstreu\\
=\\
Kein Bias
\end{frame}


\begin{frame}{Erwartungstreue und Bias bei der korrigierten Stichprobenvarianz}
Schätzer für Varianz:
\begin{align*}
\hat{V}(x) &= s^2 =\frac{1}{n-1} \sum\limits_{i=1}^{n} (x_i - \bar{x})^2
\end{align*}
Warum nicht:
$
\hat{V}(x) = s_u^2 =\frac{1}{n} \sum\limits_{i=1}^{n} (x_i - \bar{x})^2
$ ?
\end{frame}


\begin{frame}{Erwartungstreue und Bias bei der korrigierten Stichprobenvarianz}

\begin{align*}
E(s_u^2) &= E\left( \frac{1}{n} \sum\limits_{i=1}^{n} (x_i - \bar{x})^2 \right)
 = \frac{n-1}{n} \sigma^2\\[1em]
E(s^2) &= E\left( \frac{1}{n-1} \sum\limits_{i=1}^{n} (x_i - \bar{x})^2 \right)
 = \sigma^2\\[1em]
\end{align*}
Berechnen Sie den Bias von $s_u^2$ und $s^2$.\\[.8em]
\underline{Erinnerung}: $\Bias(T)=E(T)-\vartheta$\\[.2em]
\underline{Hier}: $T = s_u^2$ bzw. $s^2T = $ sowie $\vartheta = \sigma^2$
\end{frame}



\begin{frame}{Ist mein Schätzer geeignet?}{Theoretische Eigenschaften}

Es gibt verschiedene Kriterien:
\begin{itemize}
\item \textbf{Erwartungstreue}: findet mein Schätzer im Mittel den richtigen Wert?
\item \textbf{Bias}: ist mein Schätzer unverzerrt?
\item \textbf{Effizienz}: hat mein Schätzer eine geringe Varianz?
\item \textbf{Theoretischer MSE}
\end{itemize}

\end{frame}



\begin{frame}{Effizienz}{Hat mein Schätzer eine geringe Varianz?}
Ein Schätzer $T_1$ ist effizienter als ein Schätzer $T_2$ wenn gilt:
$$V(T_1) < V(T_2)$$
\end{frame}


\begin{frame}{Effizienz}{Hat mein Schätzer eine geringe Varianz?}
Bestimmen Sie die Varianz von $T_2$ und $T_3$. \\
\begin{align*}
V(T_2) &= V(X_1)  \\
V(T_3) &= V\left(\frac{2}{3} \cdot X_2\right)\\
\end{align*}
\underline{Erinnerung}: $V(T) = E(T^2) - (E(T))^2$\\[2em]
Welcher Schätzer ist effizienter?\\
\end{frame}




\begin{frame}{Effizienz}{Hat mein Schätzer eine geringe Varianz?}
Bestimmen Sie die Varianz von $T_1$, $T_2$ und $T_3$. \\
\begin{align*}
V(T_1) &= V\left(\frac{X_1+X_2}{2}\right) \\
V(T_2) &= V(X_1)  \\
V(T_3) &= V\left(\frac{2}{3} \cdot X_2\right)\\
\end{align*}
\underline{Erinnerung}: $V(T) = E(T^2) - (E(T))^2$\\[2em]
Welcher Schätzer ist am effizientesten?\\
\end{frame}

\begin{frame}

\begin{align*}
V(T_1) &= E(T_1^2) - (E(T_1))^2\\[1em]
E(T_1^2) &= E\left( \left(\frac{X_1+X_2}{2}\right)^2 \right)\\
&= E\left( \frac{1}{4}\left(X_1 + X_2\right)^2 \right)\\
&= \frac{1}{4} \cdot E\left( X_1^2 + 2 \cdot X_1 \cdot X_2 + X_2^2 \right)\\
&= \frac{1}{4} \cdot \left( 
\underbrace{ E\left( X_1^2 \right) }_{\text{bekannt}} + 
2 \cdot \underbrace{ E\left( X_1 \cdot X_2 \right) }_{\text{?}} + 
\underbrace{ E\left( X_2^2 \right) }_{\text{bekannt}}  
\right)\\
\end{align*}

\end{frame}

\begin{frame}
\begin{columns}
\begin{column}{0.6\textwidth}
		\begin{tabular}{|c|c||c|c|c|}
			\hline
			             & Personen in & & & $Z$\\
			& der Stichprobe& $X_1$ & $X_2$ & $X_1 \cdot X_2$\\
			\hline
			1 & \textcolor{red}{Anna}, \textcolor{OliveGreen}{Max} & 3 & 1 & 3\\
			\hline
			2 & \textcolor{red}{Anna}, \textcolor{blue}{Eva} & 3 & 2 & 6\\
			\hline
			3 & \textcolor{OliveGreen}{Max}, \textcolor{red}{Anna} & 1 & 3 & 3\\
			\hline
			4 & \textcolor{OliveGreen}{Max}, \textcolor{blue}{Eva} & 1 & 2 & 2\\
			\hline
			5 & \textcolor{blue}{Eva}, \textcolor{red}{Anna} & 2 & 3 & 6\\
			\hline
			6 & \textcolor{blue}{Eva}, \textcolor{OliveGreen}{Max} & 2 & 1 & 2\\
			\hline
		\end{tabular}
\end{column}
\begin{column}{0.4\textwidth}
\centering
$Z = X_1 \cdot X_2$\\[1em]
$P(Z = 2) = \frac{2}{6} = \frac{1}{3}$\\[1em]
$P(Z = 3) = \frac{2}{6} = \frac{1}{3}$\\[1em]
$P(Z = 6) = \frac{2}{6} = \frac{1}{3}$\\[1em]
\end{column}
\end{columns}

\vspace{2em}
$E\left( X_1 \cdot X_2 \right) = E\left( Z \right) =$ 
\end{frame}


\begin{frame}{Effizienz}{Hat mein Schätzer eine geringe Varianz?}
Bestimmen Sie die Varianz von $T_1$, $T_2$ und $T_3$. \\
\begin{align*}
V(T_1) &= V\left(\frac{X_1+X_2}{2}\right) &= 0.2 \\
V(T_2) &= V(X_1) &= 0.6 \\
V(T_3) &= V\left(\frac{2}{3} \cdot X_2\right) &= 0.4\\
\end{align*}
Welcher Schätzer ist am effizientesten?
\end{frame}

\begin{frame}{Warum hat ein Schätzer $T$ eine Varianz $V(T)$?}
\centering
Stichprobe zufällig \& $T$ hängt von Stichprobe ab\\[1em]
{\huge $\downarrow$}\\[1em]
$T$ zufällig
\end{frame}



\begin{frame}{Warum hat ein Schätzer $T$ eine Varianz $V(T)$?}
\centering
		\begin{tabular}{|c|c||c|c||c|c|c|}
			\hline
			&Personen in & \multicolumn{5}{|c|}{Realisationen von}\\
			Stichprobe & der Stichprobe& $X_1$ & $X_2$ & $T_1$ & $T_2$ & $T_3$\\
			\hline
			1 & \textcolor{red}{Anna}, \textcolor{OliveGreen}{Max} & \textcolor{red}{3} & \textcolor{OliveGreen}{1} & 2 & 3 & 0.6\\
			\hline
			2 & \textcolor{red}{Anna}, \textcolor{blue}{Eva} & \textcolor{red}{3} & \textcolor{blue}{2} & 2.5 & 3 & 1.3\\
			\hline
			3 & \textcolor{OliveGreen}{Max}, \textcolor{red}{Anna} & \textcolor{OliveGreen}{1} & \textcolor{red}{3} & 2 & 1 & 2\\
			\hline
			4 & \textcolor{OliveGreen}{Max}, \textcolor{blue}{Eva} & \textcolor{OliveGreen}{1} & \textcolor{blue}{2} & 1.5 & 1 & 1.3\\
			\hline
			5 & \textcolor{blue}{Eva}, \textcolor{red}{Anna} & \textcolor{blue}{2} & \textcolor{red}{3} & 2.5 & 2 & 2\\
			\hline
			6 & \textcolor{blue}{Eva}, \textcolor{OliveGreen}{Max} & \textcolor{blue}{2} & \textcolor{OliveGreen}{1} & 1.5 & 2 & 0.6\\
			\hline
		\end{tabular}
\end{frame}


\begin{frame}{Theoretischer MSE}
Der MSE bildet einen Kompromiss zwischen hoher Effizienz (geringer Varianz) und geringem Bias:
\begin{align*}
\MSE(T) &= \left(E(T - \vartheta)\right)^2\\
&= V(T) + \left(\Bias(T)\right)^2
\end{align*}
\end{frame}


\begin{frame}{Theoretischer MSE}
\begin{align*}
\MSE(T) &= \left(E(T - \vartheta)\right)^2\\
&= V(T) + \left(\Bias(T)\right)^2
\end{align*}

Bestimmen Sie den MSE von $T_1$, $T_2$ und $T_3$.
\begin{columns}
\begin{column}{0.24\textwidth}
\begin{align*}
V(T_1) & = 0.2 \\
V(T_2) & = 0.6 \\
V(T_3) & = 0.4
\end{align*}
\end{column}
%
\begin{column}{0.3\textwidth}
\begin{align*}
E(T_1) & = 2 = \mu \\
E(T_2) & = 2 = \mu \\
E(T_3) & = 1.3 \neq \mu 
\end{align*}
\end{column}
%
\begin{column}{0.35\textwidth}
\begin{align*}
\Bias(T_1) &= E\left(T_1\right) - \mu \\
\Bias(T_2) &= E(T_2) - \mu \\
\Bias(T_3) &= E\left(T_3\right) - \mu 
\end{align*}
\end{column}
\end{columns}

\end{frame}



\begin{frame}{Theoretischer MSE}
\begin{align*}
\MSE(T) &= \left(E(T - \vartheta)\right)^2\\
&= V(T) + \left(\Bias(T)\right)^2
\end{align*}

Bestimmen Sie den MSE von $T_1$, $T_2$ und $T_3$.

\begin{align*}
\MSE(T_1) & = 0.2 \\
\MSE(T_2) & = 0.6 \\
\MSE(T_3) & = 0.9 \\
\end{align*}

Welcher Schätzer ist am besten (bzgl. MSE)?
\end{frame}




\begin{frame}{Ist mein Schätzer geeignet?}{Theoretische Eigenschaften}

\begin{itemize}
\item \textbf{Erwartungstreue}
\item \textbf{Bias}
\item \textbf{Effizienz}
\item \textbf{Theoretischer MSE}
\end{itemize}

Idee: Wie verhält sich der Schätzer in allen denkbaren Stichproben?

\end{frame}


\begin{frame}{Wie verhält sich der Schätzer in allen denkbaren Stichproben?}
\centering
		\begin{tabular}{|c|c||c|c||c|c|c|}
			\hline
			&Personen in & \multicolumn{5}{|c|}{Realisationen von}\\
			Stichprobe & der Stichprobe& $X_1$ & $X_2$ & $T_1$ & $T_2$ & $T_3$\\
			\hline
			1 & \textcolor{red}{Anna}, \textcolor{OliveGreen}{Max} & \textcolor{red}{3} & \textcolor{OliveGreen}{1} & 2 & 3 & 0.6\\
			\hline
			2 & \textcolor{red}{Anna}, \textcolor{blue}{Eva} & \textcolor{red}{3} & \textcolor{blue}{2} & 2.5 & 3 & 1.3\\
			\hline
			3 & \textcolor{OliveGreen}{Max}, \textcolor{red}{Anna} & \textcolor{OliveGreen}{1} & \textcolor{red}{3} & 2 & 1 & 2\\
			\hline
			4 & \textcolor{OliveGreen}{Max}, \textcolor{blue}{Eva} & \textcolor{OliveGreen}{1} & \textcolor{blue}{2} & 1.5 & 1 & 1.3\\
			\hline
			5 & \textcolor{blue}{Eva}, \textcolor{red}{Anna} & \textcolor{blue}{2} & \textcolor{red}{3} & 2.5 & 2 & 2\\
			\hline
			6 & \textcolor{blue}{Eva}, \textcolor{OliveGreen}{Max} & \textcolor{blue}{2} & \textcolor{OliveGreen}{1} & 1.5 & 2 & 0.6\\
			\hline
		\end{tabular}
\vspace{2em}\\
Aufschreiben wie hier i.d.R. nicht möglich.\\ 
Daher theoretische Überlegungen.
\end{frame}




\begin{frame}
\includegraphics[width=.9\textwidth]{vl_13_schaetzer_eigenschaften.pdf}
\end{frame}


\end{document}